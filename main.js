function addImage(e){
	var imgUrl=$("#imgUrl").val();
	if(imgUrl.length){
		$("#imageContainer img").attr("src",imgUrl);
	}
	e.preventDefault();
}

//on pressing return, addImage() will be called
$("#urlBox").submit(addImage);

$("input[type=range]").mousemove(editImage).change(editImage);

function editImage(){
	var gs = $("#gs").val(); // grayscale
    var blur = $("#blur").val(); // blur
    var br = $("#br").val(); // brightness
    var ct = $("#ct").val(); // contrast
    var huer = $("#huer").val(); //hue-rotate
    var opacity = $("#opacity").val(); //opacity
    var invert = $("#invert").val(); //invert
    var saturate = $("#saturate").val(); //saturate
    var sepia = $("#sepia").val(); //sepia

    $("#imageContainer img").css("filter", 'grayscale(' + gs+'%) blur(' + blur +'px) brightness(' + br +'%) contrast(' + ct +'%) hue-rotate(' + huer +'deg) opacity(' + opacity +'%) invert(' + invert +'%) saturate(' + saturate +'%) sepia(' + sepia + '%)');
    $("#imageContainer img").css("-webkit-filter", 'grayscale(' + gs+'%) blur(' + blur +'px) brightness(' + br +'%) contrast(' + ct +'%) hue-rotate(' + huer +'deg) opacity(' + opacity +'%) invert(' + invert +'%) saturate(' + saturate +'%) sepia(' + sepia + '%)');
    $("#blah").css("filter", 'grayscale(' + gs+'%) blur(' + blur +'px) brightness(' + br +'%) contrast(' + ct +'%) hue-rotate(' + huer +'deg) opacity(' + opacity +'%) invert(' + invert +'%) saturate(' + saturate +'%) sepia(' + sepia + '%)');
    $("#blah").css("-webkit-filter", 'grayscale(' + gs+'%) blur(' + blur +'px) brightness(' + br +'%) contrast(' + ct +'%) hue-rotate(' + huer +'deg) opacity(' + opacity +'%) invert(' + invert +'%) saturate(' + saturate +'%) sepia(' + sepia + '%)');
}
$('#imageEditor').on('reset',function(){
	setTimeout(function(){
		editImage();
	},0);
});

//upload image
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#imageContainer img').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });

//inside filters

	$("#Filter1").click(function(){
		$("#imageContainer img").css("filter", 'brightness(1.6) contrast(1.1) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(0.9) sepia(0)');
	    $("#imageContainer img").css("-webkit-filter", 'brightness(1.6) contrast(1.1) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(0.9) sepia(0)');
	    $("#blah").css("filter", 'brightness(1.6) contrast(1.1) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(0.9) sepia(0)');
	    $("#blah").css("-webkit-filter", 'brightness(1.6) contrast(1.1) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(0.9) sepia(0)');
	    $("#gs").val(0);
	    $("#blur").val(0);
	    $("#br").val(160);
	    $("#ct").val(110);
	    $("#huer").val(0);
	    $("#opacity").val(100);
	    $("#invert").val(10);
	    $("#saturate").val(90);
	    $("#sepia").val(0);
	});

	$("#Filter2").click(function(){
		$("#imageContainer img").css("filter", 'brightness(1.3) contrast(0.8) grayscale(0.9) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#imageContainer img").css("-webkit-filter", 'brightness(1.3) contrast(0.8) grayscale(0.9) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#blah").css("filter", 'brightness(1.3) contrast(0.8) grayscale(0.9) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#blah").css("-webkit-filter", 'brightness(1.3) contrast(0.8) grayscale(0.9) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#gs").val(90);
	    $("#blur").val(0);
	    $("#br").val(130);
	    $("#ct").val(80);
	    $("#huer").val(0);
	    $("#opacity").val(100);
	    $("#invert").val(0);
	    $("#saturate").val(100);
	    $("#sepia").val(0);
	});

	$("#Filter3").click(function(){
		$("#imageContainer img").css("filter", 'brightness(1.3) contrast(1.1) grayscale(0) hue-rotate(180deg) invert(0) saturate(2) sepia(0)');
	    $("#imageContainer img").css("-webkit-filter", 'brightness(1.3) contrast(1.1) grayscale(0) hue-rotate(180deg) invert(0) saturate(2) sepia(0)');
	    $("#blah").css("filter", 'brightness(1.3) contrast(1.1) grayscale(0) hue-rotate(180deg) invert(0) saturate(2) sepia(0)');
	    $("#blah").css("-webkit-filter", 'brightness(1.3) contrast(1.1) grayscale(0) hue-rotate(180deg) invert(0) saturate(2) sepia(0)');
	    $("#gs").val(0);
	    $("#blur").val(0);
	    $("#br").val(130);
	    $("#ct").val(110);
	    $("#huer").val(180);
	    $("#opacity").val(100);
	    $("#invert").val(0);
	    $("#saturate").val(200);
	    $("#sepia").val(0);
	});

	$("#Filter4").click(function(){
		$("#imageContainer img").css("filter", 'brightness(2.4) contrast(0.8) grayscale(0) hue-rotate(0deg) invert(0.3) saturate(1) sepia(1)');
	    $("#imageContainer img").css("-webkit-filter", 'brightness(2.4) contrast(0.8) grayscale(0) hue-rotate(0deg) invert(0.3) saturate(1) sepia(1)');
	    $("#blah").css("filter", 'brightness(2.4) contrast(0.8) grayscale(0) hue-rotate(0deg) invert(0.3) saturate(1) sepia(1)');
	    $("#blah").css("-webkit-filter", 'brightness(2.4) contrast(0.8) grayscale(0) hue-rotate(0deg) invert(0.3) saturate(1) sepia(1)');
	    $("#gs").val(0);
	    $("#blur").val(0);
	    $("#br").val(240);
	    $("#ct").val(80);
	    $("#huer").val(0);
	    $("#opacity").val(100);
	    $("#invert").val(30);
	    $("#saturate").val(100);
	    $("#sepia").val(100);
	});

	$("#Filter5").click(function(){
		$("#imageContainer img").css("filter", 'brightness(1.3) contrast(1.4) grayscale(0) hue-rotate(0deg) invert(0.07) saturate(0.5) sepia(0)');
	    $("#imageContainer img").css("-webkit-filter", 'brightness(1.3) contrast(1.4) grayscale(0) hue-rotate(0deg) invert(0.07) saturate(0.5) sepia(0)');
	    $("#blah").css("filter", 'brightness(1.3) contrast(1.4) grayscale(0) hue-rotate(0deg) invert(0.07) saturate(0.5) sepia(0)');
	    $("#blah").css("-webkit-filter", 'brightness(1.3) contrast(1.4) grayscale(0) hue-rotate(0deg) invert(0.07) saturate(0.5) sepia(0)');
	    $("#gs").val(0);
	    $("#blur").val(0);
	    $("#br").val(130);
	    $("#ct").val(140);
	    $("#huer").val(0);
	    $("#opacity").val(100);
	    $("#invert").val(7);
	    $("#saturate").val(50);
	    $("#sepia").val(0);
	});

	$("#Filter6").click(function(){
		$("#imageContainer img").css("filter", 'brightness(1.3) contrast(1.2) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(1.8) sepia(0)');
	    $("#imageContainer img").css("-webkit-filter", 'brightness(1.3) contrast(1.2) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(1.8) sepia(0)');
	    $("#blah").css("filter", 'brightness(1.3) contrast(1.2) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(1.8) sepia(0)');
	    $("#blah").css("-webkit-filter", 'brightness(1.3) contrast(1.2) grayscale(0) hue-rotate(0deg) invert(0.1) saturate(1.8) sepia(0)');
	    $("#gs").val(0);
	    $("#blur").val(0);
	    $("#br").val(130);
	    $("#ct").val(120);
	    $("#huer").val(0);
	    $("#opacity").val(100);
	    $("#invert").val(10);
	    $("#saturate").val(180);
	    $("#sepia").val(0);
	});

	$("#Filter0").click(function(){
		$("#imageContainer img").css("filter", 'brightness(1) contrast(1) grayscale(0) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#imageContainer img").css("-webkit-filter", 'brightness(1) contrast(1) grayscale(0) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#blah").css("filter", 'brightness(1) contrast(1) grayscale(0) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#blah").css("-webkit-filter", 'brightness(1) contrast(1) grayscale(0) hue-rotate(0deg) invert(0) saturate(1) sepia(0)');
	    $("#gs").val(0);
	    $("#blur").val(0);
	    $("#br").val(100);
	    $("#ct").val(100);
	    $("#huer").val(0);
	    $("#opacity").val(100);
	    $("#invert").val(0);
	    $("#saturate").val(100);
	    $("#sepia").val(0);
	});
