CSS Photo Filter

•	Realized a photo filter written with CSS and jQuery. It allows users to edit their photos with various photo filters to edit photos such as improving lighting and color

•	Provided two ways for users to upload their photos - uploading their own photos from computers or choosing images from web address

•	Provided several built-in filters for users to edit their photos